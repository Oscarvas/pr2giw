<?php
/*
Asignatura: Gestion de la Informacion en la Web.
Practica:   4.
Grupo nº:   10.
Autores:
    Naji, Shahad.
    Pérez, Alexandra.
    Pax, Rafael.
    Vasquez, Oscar David.
El codigo a continuacion es fruto unica y exclusivamente del trabajo de los autores declarados anteriormente.
*/
	define("DBHost", "localhost");
	define("DBUser", "root");
	define("DBPass", "root");
	define("DB", "GIW_grupo10");
	define("SEARCH_TERM_OBRA", "obras");
	define("SEARCH_TERM_TEATRO", "teatros");
	define("SEARCH_TERM_ALL", "all");
	define("VALID_FLAG", "valid");
	define("TEATRO_STR", "teatro");
	define("USERNAME_STR", "username");

?>