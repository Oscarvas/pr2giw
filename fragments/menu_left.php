<?php
/*
 * Asignatura: Gestion de la Informacion en la Web.
 * Practica: 4.
 * Grupo nº: 10.
 * Autores:
 * Naji, Shahad.
 * Pérez, Alexandra.
 * Pax, Rafael.
 * Vasquez, Oscar David.
 * El codigo a continuacion es fruto unica y exclusivamente del trabajo de los autores declarados anteriormente.
 */
include_once 'Constants.php';
?>
<td class="left">&nbsp;
    <ul class="links">
        <li><a href="/src/index.php" class="selected">Home</a></li>
         <?php if(isset($_SESSION['googleAuth'])) {?>
        <li><a style="background-color: gray;" href="#">Modificar perfil</a></li>
        <?php }else{?>
        <li><a href="/src/users/register.php">Modificar perfil</a></li>
        <?php }?>
        <li><a
            href="/src/actions/busqueda.php?type=<?=SEARCH_TERM_ALL?>">Listado</a></li>        
        <?php if($_SESSION["rol"] == "Administrador"){?>
        <li><a href="/src/actions/edita.php">Nuevo Teatro</a></li>
        <?php } else { ?>
        <li><a href="/src/actions/MisEntradas.php">Mis entradas</a></li>
        <?php } ?>
       <!--  <li><a href="/src/users/register.php">Nuevo usuario</a></li> -->
        <li><a href="/src/users/logout.php">Salir</a></li>
    </ul>
    <table class="content">
        <tr>
            <td>
                <form action="/src/actions/busqueda.php" method="get">
                    <!-- formulario de busqueda de teatro -->
                    <input type="text" placeholder="Nombre del teatro"
                        name="term" required /> <input type="hidden"
                        value='<?=SEARCH_TERM_TEATRO?>' name="type" /> <input
                        type="submit" value="Buscar teatros"
                        name="submit" />
                </form>
                <form action="/src/actions/busqueda.php" method="get">
                    <!-- formulario de busqueda por obra-->
                    <input type="text" placeholder="Nombre de la obra"
                        name="term" required /> <input type="hidden"
                        value='<?=SEARCH_TERM_OBRA?>' name="type" /> <input
                        type="submit" value="Buscar obras" name="submit" />

                </form>
            </td>
        </tr>

    </table>
</td>