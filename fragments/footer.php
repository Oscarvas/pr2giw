<!--
Asignatura: Gestion de la Informacion en la Web.
Practica:   4.
Grupo nº:   10.
Autores:
    Naji, Shahad.
    Pérez, Alexandra.
    Pax, Rafael.
    Vasquez, Oscar David.
El codigo a continuacion es fruto unica y exclusivamente del trabajo de los autores declarados anteriormente.
-->
<table class="footer">
    <tr>
        <td width="100%"><font size="1">&nbsp;</font></td>
    </tr>
</table>
<script src="/src/js/tablesorter/jquery-latest.js"></script>

<script src="/src/js/tablesorter/jquery.tablesorter.js"></script>
<script src="/src/js/main.js"></script>