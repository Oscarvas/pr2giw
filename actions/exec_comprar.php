<?php
/*
Asignatura: Gestion de la Informacion en la Web.
Practica:   4.
Grupo nº:   10.
Autores:
    Naji, Shahad.
    Pérez, Alexandra.
    Pax, Rafael.
    Vasquez, Oscar David.
El codigo a continuacion es fruto unica y exclusivamente del trabajo de los autores declarados anteriormente.
*/
include_once '../fragments/Constants.php';
include_once '../classes/LogInUtils.php';
include_once '../database/DBHelper.php';
include_once '../classes/Inputs.php';

$Id=Inputs::sanitizeInput($_REQUEST['id']);
$fila= Inputs::sanitizeDigitos($_REQUEST['fila']);
$asiento= Inputs::sanitizeDigitos($_REQUEST['asiento']);
if (Inputs::checkHora($_REQUEST['sesion'])) $sesion=$_REQUEST['sesion'];
if (Inputs::checkFecha($_REQUEST['fecha'])) $dia=$_REQUEST['fecha'];
$accion= Inputs::sanitizeInput($_REQUEST['accion']);
if (Inputs::checkEmail($_REQUEST['email'])) $email=$_REQUEST['email'];
$objDb = new DBHelper();

if($accion=='libre'){
	$data = [
	'Id_teatro' => new MongoId($Id),
	'sesion'=>$sesion,
	'fila'=>$fila,
	'asiento'=>$asiento,
	'dia'=>$dia,
	'email'=>$email ];
	$objDb->compra($data);
}
else if($accion=='ocupado'){
	$data = [
	'Id_teatro' =>new MongoId($Id),
	'sesion'=>$sesion,
	'fila'=>$fila,
	'asiento'=>$asiento,
	'dia'=>$dia ];

	$objDb->venta($data);
	
}
 
header("LOCATION: comprar.php?teatro=".$Id."&sesion=".$sesion."&fecha=".$dia."");


?> 