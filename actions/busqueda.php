<?php
/*
Asignatura: Gestion de la Informacion en la Web.
Practica:   4.
Grupo nº:   10.
Autores:
    Naji, Shahad.
    Pérez, Alexandra.
    Pax, Rafael.
    Vasquez, Oscar David.
El codigo a continuacion es fruto unica y exclusivamente del trabajo de los autores declarados anteriormente.
*/
require_once '../users/sessions.php';
include_once '../classes/Inputs.php';
$objses = new Sessions();
$objses->init();

$user = isset($_SESSION['user']) ? $_SESSION['user'] : '' ;

if($user == ''){
    header('Location: ../users/login.php');
}

?>
<?php
//session_start ();
include_once '../classes/TeatroBD.php';
include_once '../classes/LogInUtils.php';

/*
 * Comprobamos que los params son correctos. Si no, redireccionamos
 */
if (! isset ( $_GET ['type'] ) || ($_GET ['type'] != SEARCH_TERM_ALL && $_GET ['type'] != SEARCH_TERM_TEATRO && $_GET ['type'] != SEARCH_TERM_OBRA)) {
    header ( 'Location: ../index.php' );
    die ();
}
$term = "";
$type = Inputs::sanitizeInput($_GET ['type']);
if (isset ( $_GET ['term'] ))
    $term = Inputs::sanitizeInput($_GET ['term']);

$BUSQUEDA_HEADER = "";
if ($type == SEARCH_TERM_TEATRO) {
    $BUSQUEDA_HEADER = "B&uacute;squeda de teatros";
} else if ($type == SEARCH_TERM_OBRA) {
    $BUSQUEDA_HEADER = "B&uacute;squeda de obras";
}

// cargamos array de resultados de busqueda
$results = [ ];
if ($type != SEARCH_TERM_ALL)
    $results = TeatroBD::search ( $term, $type );
else
    $results = TeatroBD::getAllTeatros ();
?>

<?php include_once'../fragments/header.php';?>

<body>

    <?php include_once'../fragments/site_title.php';?>
    <table class="main">
        <tr>
            <?php include_once'../fragments/menu_left.php';?>
            <td class="right">
                <h1><?=$BUSQUEDA_HEADER?></h1>
                <?php if (sizeof($results)==0) {?>
                <p>No se han encontrado resultados con el t&eacute;rmino de b&uacute;squeda "<?=$term?>":</p> 
                <?php } else { ?>
                <?php if ($type!=SEARCH_TERM_ALL) {?>
                <p>Estos son los resultados que se han encontrado con el t&eacute;rmino de b&uacute;squeda "<?=$term?>":</p>
                <?php }?>
                <table class="content">
                    <thead>
                        <tr>
                            <th>Nombre teatro</th>
                            <th>Obra</th>
                            <th>Descripci&oacute;n</th>
                            <th>Operaciones</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php foreach ( $results as $key => $value ) { ?>
                        <tr>
                            <td> <?=$value->getNombre_teatro()?></td>
                            <td> <?=$value->getNombre_obra()?> </td>
                            <td> <?=$value->getDescripcion()?> </td>
                            <?php if (LogInUtils::isValidUser($_SESSION)) {?>
                            <td>
                                <form action="consulta.php" method="get">
                                    <input type="hidden" name="<?=TEATRO_STR?>"
                                        value="<?=$value->getId()?>" />
                                    <input type="submit" name="button"
                                        value="Consulta" />
                                </form>
                            </td>

                            <td>
                                <form action="comprar.php" method="get">
                                    <input type="hidden" name="<?=TEATRO_STR?>"
                                        value="<?=$value->getId()?>" />
                                    <?php if(Inputs::sanitizeInput($_SESSION["rol"]) == "Cliente"){ ?>
                                    <input type="submit" name="button"
                                        value="Comprar" />
                                    <?php } elseif (Inputs::sanitizeInput($_SESSION["rol"])=="Administrador") { ?>
                                    <input type="submit" name="button"
                                        value="Reservados" />
                                    <?php } ?>
                                </form>
                            </td>
                         <?php if (Inputs::sanitizeInput($_SESSION["rol"]) == "Administrador"){ ?>
                            <td>
                                <form action="edita.php" method="get">
                                    <input type="hidden"name="<?=TEATRO_STR?>"
                                        value="<?=$value->getId()?>" />
                                    <input type="submit" name="button"
                                        value="Editar" />
                                </form>
                            </td>
                            
                            <td>
                                <form action="borrar.php" method="POST">
                                    <input type="hidden" name="<?=TEATRO_STR?>"
                                        value="<?=$value->getId()?>" />
                                    <input type="submit" name="button"
                                        value="Borrar" />
                                </form>
                            </td>

                            <?php }}?>
                        </tr>
                    <?php }?>
                    </tbody>
                </table>
                <?php }?>
            </td>
            <td width="21%" valign="top"></td>
        </tr>
    </table>
    <?php include_once'../fragments/footer.php';?>


</body>

</html>



