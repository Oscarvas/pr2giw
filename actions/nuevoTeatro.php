<?php 
/*
Asignatura: Gestion de la Informacion en la Web.
Practica:   4.
Grupo nº:   10.
Autores:
    Naji, Shahad.
    Pérez, Alexandra.
    Pax, Rafael.
    Vasquez, Oscar David.
El codigo a continuacion es fruto unica y exclusivamente del trabajo de los autores declarados anteriormente.
*/
include_once '../fragments/header.php';?>

<body>

    <?php include_once '../fragments/site_title.php';
    include_once '../fragments/Constants.php';?>
    <table class="main">
        <tr>
            <?php include_once '../fragments/menu_left.php';?>
            <td class="right">
                <h1>Nuevo teatro</h1>

                <table class="content">
                    <tr>
                        <td>
                            <form action="busqueda.php" type="post">
                                <input type="text"
                                    placeholder="Nombre del teatro"
                                    name="term" required /> <input
                                    type="hidden"
                                    value='<?=SEARCH_TERM_TEATRO?>'
                                    name="type" /> <input type="submit"
                                    value="Buscar teatros" name="submit" />
                            </form>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <form action="busqueda.php" type="post">
                                <input type="text"
                                    placeholder="Nombre de la obra"
                                    name="term" required /> <input
                                    type="hidden"
                                    value='<?=SEARCH_TERM_OBRA?>'
                                    name="type" /> <input type="submit"
                                    value="Buscar obras" name="submit" />
                            </form>
                        </td>
                    </tr>
                   
                </table>
            </td>
            <td width="21%" valign="top"></td>
        </tr>
    </table>
    <?php include_once '../fragments/footer.php';?>


</body>

</html>
