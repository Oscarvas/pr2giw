<?php
/*
Asignatura: Gestion de la Informacion en la Web.
Practica:   4.
Grupo nº:   10.
Autores:
    Naji, Shahad.
    Pérez, Alexandra.
    Pax, Rafael.
    Vasquez, Oscar David.
El codigo a continuacion es fruto unica y exclusivamente del trabajo de los autores declarados anteriormente.
*/
require_once '../users/sessions.php';
include_once '../classes/Inputs.php';
$objses = new Sessions();
$objses->init();

$user = isset($_SESSION['user']) ? Inputs::sanitizeInput($_SESSION['user']) : null ;

if($user == ''){
    header('Location: ../users/login.php');
}
if (Inputs::checkEmail($_SESSION['email']) ) $email=$_SESSION['email'];
include_once '../fragments/Constants.php';
include_once '../classes/TeatroBD.php';
include_once '../classes/ValoracionBD.php';
$idTeatro = Inputs::sanitizeInput($_REQUEST[TEATRO_STR]);
$teatro = TeatroBD::getTeatro ( $idTeatro );
if(isset($_REQUEST["sesion"]) && Inputs::checkHora($_REQUEST["sesion"])){
    $sesion=$_REQUEST["sesion"];
}else{
    $sesion="";
}
if(isset($_REQUEST["fecha"]) && Inputs::checkFecha($_REQUEST["fecha"])){
    $fecha=$_REQUEST["fecha"];
}else{
    $fecha=date("Y-m-d");
}
if ($sesion=="") {
    $sesion=$teatro->getSesion1();
}
$nume_fila=$teatro->getNume_fila();
$nume_asientos=$teatro->getNume_asientos();
include_once '../fragments/header.php';


?>
<body>
<?php include_once '../fragments/site_title.php';?>
    <table class='main'>
        <tr>
            
            <?php include_once '../fragments/menu_left.php';?>
            
            <td class='right'>
                <h1>Comprar entrada</h1>

                <form action='comprar.php' method='POST'>
                    <input type='hidden' name='id' value="<?=$idTeatro?>" />
                    <input type='hidden' name='<?=TEATRO_STR?>' value="<?=$idTeatro?>" />
                    <table>
                        
                        <tr>
                            <th>Nombre teatro</th>
                            <td><?=$teatro->getNombre_teatro()?></td>
                        </tr>
                        <tr>
                            <th>Nombre obra</th>
                            <td><?=$teatro->getNombre_obra()?></td>
                        </tr>
                        <tr>
                            <th>Sesi&oacute;n</th>
                            <td>
                                <select name='sesion'>
                                    <?php if ($sesion==$teatro->getSesion1()){ ?>
                                    <option value="<?=$teatro->getSesion1()?>" selected><?=$teatro->getSesion1()?></option>
                                    <?php }else{ ?>
                                    <option value="<?=$teatro->getSesion1()?>"><?=$teatro->getSesion1()?></option>
                                    <?php }if($sesion==$teatro->getSesion2()){ ?>
                                    <option value="<?=$teatro->getSesion2()?>" selected><?=$teatro->getSesion2()?></option>
                                    <?php }else{ ?>                           
                                    <option value="<?=$teatro->getSesion2()?>"><?=$teatro->getSesion2()?></option>
                                    <?php }if($sesion==$teatro->getSesion3()){ ?>
                                    <option value="<?=$teatro->getSesion3()?>" selected><?=$teatro->getSesion3()?></option>
                                    <?php }else{ ?>
                                    <option value="<?=$teatro->getSesion3()?>"><?=$teatro->getSesion3()?></option>
                                    <?php } ?>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <th>D&iacute;a</th>
                            <td>
                                <input type='text' name='fecha' value=<?=$fecha?>>  
                            </td>
                            <td>
                                <input type='submit' value='Cambiar sesi&oacute;n'>
                            </td>
                        </tr>
                    </table>
                </form>                    
                    <table>
                        <h3 align="center">Pantalla</h3>
                
                        <?php for($i = 1; $i <= $teatro->getNume_fila(); $i ++) { ?>
                        <tr>
                            <?php   for($j = 1; $j <= $teatro->getNume_asientos(); $j ++) { ?>
                            <td>

                                <?php 
                                
                                $numero=$teatro->asiento($i, $j, $sesion,$fecha );
                                if(strcmp($numero, 'no')==0){

                                    $accion='libre';
                                }
                                else{
                                    if(strcmp($numero, $email) == 0){
                                        $accion='ocupado';
                                    }
                                    else{
                                        $accion='noDisponible';
                                    }
                                }                                
                            ?>

                                <form action='exec_comprar.php' method='POST'>
                                    
                                    <input type='hidden' name='id' value="<?=$idTeatro?>" />
                                    <input type='hidden' name='fila' value="<?=$i?>" />
                                    <input type='hidden' name='asiento' value="<?=$j?>" />
                                    <input type='hidden' name='sesion' value="<?=$sesion?>" />
                                    <input type='hidden' name='fecha' value="<?=$fecha?>" />
                                    <input type='hidden' name='accion' value="<?=$accion?>" />
                                    <input type='hidden' name='email' value="<?=$email?>" />


                                    <?php if ($_SESSION['rol'] == "Administrador") {?> 
                                        <input type='submit' class='<?=$accion?>' value='' disabled>
                                    <?php } else {?>
                                        <input type='submit' class='<?=$accion?>' value=''>
                                    <?php } ?>
                                </form>
                            </td>
                        <?php } ?>
                        </tr>
                        <?php } ?>
                            
            </td>

        </tr>       
                    </table>
 <?php include_once'../fragments/footer.php';?>
</body>




