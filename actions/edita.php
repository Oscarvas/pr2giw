<?php
/*
Asignatura: Gestion de la Informacion en la Web.
Practica:   4.
Grupo nº:   10.
Autores:
    Naji, Shahad.
    Pérez, Alexandra.
    Pax, Rafael.
    Vasquez, Oscar David.
El codigo a continuacion es fruto unica y exclusivamente del trabajo de los autores declarados anteriormente.
*/
require_once '../users/sessions.php';
include_once '../classes/Inputs.php';
$objses = new Sessions();
$objses->init();

$user = isset($_SESSION['user']) ? Inputs::sanitizeInput($_SESSION['user']) : '' ;

if($user == ''){
    header('Location: ../users/login.php');
}

?>
<?php
include_once '../fragments/Constants.php';
include_once '../classes/TeatroBD.php';

$teatro = NULL;
if (isset ( $_GET [TEATRO_STR] )) {
	$idTeatro = Inputs::sanitizeInput($_GET [TEATRO_STR]);
	$teatro = TeatroBD::getTeatro ( $idTeatro );
}

$Id = - 1;
$nombre_teatro = "";
$nombre_obra = "";
$descripcion = "";
$sesion1 = "12:00";
$sesion2 = "16:00";
$sesion3 = "18:00";
$nume_filas = 10;
$nume_asientos = 10;

if ($teatro != NULL) {
	$Id = $teatro->getId ();
	$nombre_teatro = $teatro->getNombre_teatro ();
	$nombre_obra = $teatro->getNombre_obra ();
	$descripcion = $teatro->getDescripcion ();
	$sesion1 = $teatro->getSesion1 ();
	$sesion2 = $teatro->getSesion2 ();
	$sesion3 = $teatro->getSesion3 ();
	$nume_filas = $teatro->getNume_fila ();
	$nume_asientos = $teatro->getNume_asientos ();
}
?>
<?php include_once '../fragments/header.php';?>

<body>

    <?php include_once '../fragments/site_title.php';?>
    <table class="main">
        <tr>
            <?php include_once '../fragments/menu_left.php';?>
            <td class="right">
                <form action="modify.php" method="POST">
                    <input type="hidden" name="Id" value="<?=$Id?>">
                    <table class="content">
                        <thead>
                            <tr>
                                <th>Datos del teatro</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <th>Nombre del teatro:(*)</th>
                                <td><input type="text"
                                    name="nombre_teatro"
                                    value="<?=$nombre_teatro?>" required></td>
                            </tr>
                            <tr>
                                <th>Nombre de la obra:(*)</th>
                                <td><input type="text"
                                    name="nombre_obra"
                                    value="<?=$nombre_obra?>" required></td>
                            </tr>
                            <tr>
                                <th>Descripci&oacute;n:</th>
                                <td><input type="text"
                                    name="descripcion"
                                    value="<?=$descripcion?>"></td>
                            </tr>
                            <tr>
                                <th>Sesi&oacute;n 1:</th>
                                <td><input type="text" name="sesion1"
                                    value="<?=$sesion1?>"></td>
                            </tr>
                            <tr>
                                <th>Sesi&oacute;n 2:</th>
                                <td><input type="text" name="sesion2"
                                    value="<?=$sesion2?>"></td>
                            </tr>
                            <tr>
                                <th>Sesi&oacute;n 3:</th>
                                <td><input type="text" name="sesion3"
                                    value="<?=$sesion3?>"></td>
                            </tr>
                            <tr>
                                <th>Numero de filas:</th>
                                <td><input type="text" name="nfilas"
                                    value="<?=$nume_filas?>"></td>
                            </tr>
                            <tr>
                                <th>Numero de asientos:</th>
                                <td><input type="text" name="nasientos"
                                    value="<?=$nume_asientos?>"></td>
                            </tr>


                            </td>
                        </tbody>
                    </table>
                    <input type="submit"
                        value="Modificar / A&ntilde;adir Teatro" />
                </form>
                <table>
                    <tr>
                        <td width="21%" valign="top"></td>
                    </tr>

                </table>

    </table>
    <?php include_once '../fragments/footer.php';?>

</body>

</html>
