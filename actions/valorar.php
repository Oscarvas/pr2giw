<?php
/*
Asignatura: Gestion de la Informacion en la Web.
Practica:   4.
Grupo nº:   10.
Autores:
    Naji, Shahad.
    Pérez, Alexandra.
    Pax, Rafael.
    Vasquez, Oscar David.
El codigo a continuacion es fruto unica y exclusivamente del trabajo de los autores declarados anteriormente.
*/
require_once '../users/sessions.php';
include_once '../classes/LogInUtils.php';
include_once '../classes/Inputs.php';
$objses = new Sessions();
$objses->init();

$user = isset($_SESSION['user']) ? Inputs::sanitizeInput($_SESSION['user']) : null ;

if($user == ''){
    header('Location: ../users/login.php');
}
include_once'../fragments/header.php';

$nTeatro='';
if (Inputs::checkTeaLen($_REQUEST['nombre_teatro'])) $nTeatro = $_REQUEST['nombre_teatro'];
$nObra='';
if (Inputs::checkTeaLen($_REQUEST['nombre_obra'])) $nObra = $_REQUEST['nombre_obra'];
?>

<body>
	<?php include_once'../fragments/site_title.php';?>
	<table class="main">
		<tr>
			<?php include_once'../fragments/menu_left.php';
			include_once '../classes/ValoracionBD.php'; ?>
			<td class="right">
				<form method="POST" action="exec_valoracion.php">
					<input type="hidden" name="Id_teatro"value="<?=$_REQUEST['Id_teatro']?>" >
					<input type="hidden" name="dni"value="<?=$_SESSION['dni']?>" >
					
					<h2>Valorar</h2>
					<table>
						<caption>
							<h1><?=$nTeatro?>: <?=$nObra?></h1>
						</caption>
						<tr>
							<th>Puntuaci&oacute;n</th>
							<td><input type="number" name= "puntuacion" min="1" max="5" required></td>
						</tr>
						<tr>
							<th>Comentario</th>
							<td><textarea name="comentario"rows="4" cols="50" required></textarea></td>
						</tr>
						<tr>
							<td><button type="reset">Borrar formulario</button></td>
							<td><button type="submit">Guardar valoraci&oacute;n</button></td>
						</tr>
				
					
					</table>
			
				</form>
			</td>
		</tr>
	</table>



</body>