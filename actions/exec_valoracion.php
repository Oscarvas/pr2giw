<?php 
/*
Asignatura: Gestion de la Informacion en la Web.
Practica:   4.
Grupo nº:   10.
Autores:
    Naji, Shahad.
    Pérez, Alexandra.
    Pax, Rafael.
    Vasquez, Oscar David.
El codigo a continuacion es fruto unica y exclusivamente del trabajo de los autores declarados anteriormente.
*/
include_once '../classes/ValoracionBD.php';
include_once '../classes/Inputs.php';

$dni ='';
$punt ='';
$coment = '';

if (Inputs::checkDni($_POST['dni'])) $dni = $_POST['dni'];
if (Inputs::checkPuntuacion($_POST['puntuacion'])) $punt = $_POST['puntuacion'];
if (Inputs::checkTextLen($_POST['comentario'])) $coment = $_POST['comentario'];

$valoracion = new ValoracionBD("-1", 
								Inputs::sanitizeInput($_POST['Id_teatro']),
								$dni,
								date("Y-m-d"),
								$punt,
								$coment);
$valoracion->addValoracion();

header("Location: MisEntradas.php");
?>