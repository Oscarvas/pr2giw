<?php
/*
Asignatura: Gestion de la Informacion en la Web.
Practica:   4.
Grupo nº:   10.
Autores:
    Naji, Shahad.
    Pérez, Alexandra.
    Pax, Rafael.
    Vasquez, Oscar David.
El codigo a continuacion es fruto unica y exclusivamente del trabajo de los autores declarados anteriormente.
*/
include_once '../fragments/Constants.php';
include_once '../classes/TeatroBD.php';
include_once '../classes/Inputs.php';
if (isset ( $_POST [TEATRO_STR] ))
	TeatroBD::deleteTeatro ( Inputs::sanitizeInput($_POST ['teatro']) );
header ( 'Location: ../index.php' );
die ();
?>