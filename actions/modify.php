<?php
/*
 * Asignatura: Gestion de la Informacion en la Web.
 * Practica: 4.
 * Grupo nº: 10.
 * Autores:
 * Naji, Shahad.
 * Pérez, Alexandra.
 * Pax, Rafael.
 * Vasquez, Oscar David.
 * El codigo a continuacion es fruto unica y exclusivamente del trabajo de los autores declarados anteriormente.
 */
include_once '../classes/TeatroBD.php';
include_once '../classes/Inputs.php';

$id = Inputs::sanitizeInput ( $_POST ['Id'] );
$nTeatro = '';
if (Inputs::checkTeaLen ( $_POST ['nombre_teatro'] ))
	$nTeatro = $_POST ['nombre_teatro'];
$nObra = '';
if (Inputs::checkTextLen ( $_POST ['nombre_obra'] ))
	$nObra = $_POST ['nombre_obra'];
$desc = '';
if (Inputs::checkTextLen ( $_POST ['descripcion'] ))
	$desc = $_POST ['descripcion'];
$s1 = '';
if (Inputs::checkHora ( $_POST ['sesion1'] ))
	$s1 = $_POST ['sesion1'];
$s2 = '';
if (Inputs::checkHora ( $_POST ['sesion2'] ))
	$s2 = $_POST ['sesion2'];
$s3 = '';
if (Inputs::checkHora ( $_POST ['sesion3'] ))
	$s3 = $_POST ['sesion3'];
$nfilas = '';
if (Inputs::checkDigLen ( $_POST ['nfilas'] ))
	$nfilas = $_POST ['nfilas'];
$nasientos = '';
if (Inputs::checkDigLen ( $_POST ['nasientos'] ))
	$nasientos = $_POST ['nasientos'];

if ($id != '' && $nTeatro != '' && $nObra != '' && $desc != '' && $s1 != '' && $s2 != '' && $s3 != '' && $nfilas != '' && $nasientos != '') {
	
	$teatro = new TeatroBD ( $id, $nTeatro, $nObra, $desc, $s1, $s2, $s3, $nfilas, $nasientos );
} else {
	exit ( 'Datos incorrectos' );
}
TeatroBD::addOrModifyTeatro ( $teatro );
header ( 'Location: busqueda.php?type=all' );
die ();
?>