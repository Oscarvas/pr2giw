<?php
/*
Asignatura: Gestion de la Informacion en la Web.
Practica:   4.
Grupo nº:   10.
Autores:
    Naji, Shahad.
    Pérez, Alexandra.
    Pax, Rafael.
    Vasquez, Oscar David.
El codigo a continuacion es fruto unica y exclusivamente del trabajo de los autores declarados anteriormente.
*/
require_once '../users/sessions.php';
include_once '../classes/Inputs.php';
$objses = new Sessions();
$objses->init();

$user = isset($_SESSION['user']) ? Inputs::sanitizeInput($_SESSION['user']) : null ;

if($user == ''){
    header('Location: ../users/login.php');
}

?>
<?php
include_once '../fragments/Constants.php';
include_once '../classes/TeatroBD.php';
include_once '../classes/ValoracionBD.php';
if (! isset ( $_GET [TEATRO_STR] )) {
    header ( 'Location: ../index.php' );
    die ();
}
$idTeatro = Inputs::sanitizeInput($_GET [TEATRO_STR]);
$teatro = TeatroBD::getTeatro ( $idTeatro );
?>
<?php include_once '../fragments/header.php';?>

<body>

    <?php include_once '../fragments/site_title.php';?>
    <table class="main">
        <tr>
            <?php include_once '../fragments/menu_left.php';?>
            <td class="right">
                <table class="content">
                    <thead>
                        <tr>
                            <th>Datos del teatro</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <th>Nombre del teatro:</th>
                            <td><?=$teatro->getNombre_teatro()?></td>
                        </tr>
                        <tr>
                            <th>Nombre de la obra:</th>
                            <td><?=$teatro->getNombre_obra()?></td>
                        </tr>
                        <tr>
                            <th>Descripci&oacute;n:</th>
                            <td><?=$teatro->getDescripcion()?></td>
                        </tr>
                        <tr>
                            <th>Sesi&oacute;n 1:</th>
                            <td><?=$teatro->getSesion1()?></td>
                        </tr>
                        <tr>
                            <th>Sesi&oacute;n 2:</th>
                            <td><?=$teatro->getSesion2()?></td>
                        </tr>
                        <tr>
                            <th>Sesi&oacute;n 3:</th>
                            <td><?=$teatro->getSesion3()?></td>
                        </tr>
                        <tr>
                            <th>Numero de filas:</th>
                            <td><?=$teatro->getNume_fila()?></td>
                        </tr>
                        <tr>
                            <th>Numero de asientos:</th>
                            <td><?=$teatro->getNume_asientos()?></td>
                        </tr>
                       <tr>
                        <table id="myTable" class="tablesorter">
            <h1>Valoraciones: </h2>
            <thead>
                <tr>
                    <!-- <th>DNI</th> -->
                    <th>Fecha</th>
                    <th>Puntuaci&oacute;n</th>
                    <th>Comentario</th>
                </tr>
            </thead>
            <tbody>

            <?php 
            $variable = $teatro->getArray_valoracion();
            foreach ($variable as $key => $value) { ?>
                <tr>
                    <!-- <td><?=$value->getDni()?></td> -->
                    <td><?=$value->getDia()?></td>
                    <td><?=$value->getPuntuacion()?></td>
                    <td><?=$value->getComentario()?></td>
                </tr>
            <?php } ?>
            </tbody>

        </table>
                       </tr>

                        </td>
                    </tbody>
                </table>
                <table>
                    <tr>
                        <td width="21%" valign="top"></td>
                    </tr>

                </table>
    <?php include_once '../fragments/footer.php';?>

</body>

</html>
