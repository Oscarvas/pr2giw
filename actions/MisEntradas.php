<?php
/*
Asignatura: Gestion de la Informacion en la Web.
Practica:   4.
Grupo nº:   10.
Autores:
    Naji, Shahad.
    Pérez, Alexandra.
    Pax, Rafael.
    Vasquez, Oscar David.
El codigo a continuacion es fruto unica y exclusivamente del trabajo de los autores declarados anteriormente.
*/
require_once '../users/sessions.php';
include_once '../classes/LogInUtils.php';
include_once '../classes/Inputs.php';
$objses = new Sessions();
$objses->init();

$user = isset($_SESSION['user']) ? Inputs::sanitizeInput($_SESSION['user']) : null ;

if($user == ''){
    header('Location: ../users/login.php');
}
include_once'../fragments/header.php';
?>

<body>
	<?php include_once'../fragments/site_title.php';?>
	<table class="main">
		<tr>
			<?php include_once'../fragments/menu_left.php';?>
			<td class="right">
				<table>
					<tr>
						<th>Nombre teatro</th>
						<th>Nombre Obra</th>
						<th>Sesi&oacute;n</th>
						<th>Fila</th>
						<th>Asiento</th>
						<th>D&iacute;a</th>

					</tr>

					<?php
					$variable=LogInUtils::misEntradas();
					$aux = -1;
					foreach ($variable as $key => $value) { ?>
					<tr>
						<td><?=$value->getNombre_teatro()?></td>
						<td><?=$value->getNombre_obra()?></td>
						<td><?=$value->getSesion()?></td>
						<td><?=$value->getFila()?></td>
						<td><?=$value->getAsiento()?></td>
						<td><?=$value->getDia()?></td>
						<?php if  (($aux != $value->getId_teatro()->{'$id'}) && (LogInUtils::heValorado($value->getId_teatro()) == 0) ) { 
							$aux = $value->getId_teatro(); ?>

							<td class="formatoBoton"> <a  href="valorar.php?Id_teatro=<?=$value->getId_teatro()?>&nombre_teatro=<?=$value->getNombre_teatro()?>&nombre_obra=<?=$value->getNombre_obra()?>">Valorar</a></td>
					    <?php	} ?>
						
						
				<?php	} ?>
				
					

				</table>
			</td>
		</tr>
	</table>



</body>
