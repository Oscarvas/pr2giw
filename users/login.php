<?php
/*
 * Asignatura: Gestion de la Informacion en la Web.
 * Practica: 4.
 * Grupo nº: 10.
 * Autores:
 * Naji, Shahad.
 * Pérez, Alexandra.
 * Pax, Rafael.
 * Vasquez, Oscar David.
 * El codigo a continuacion es fruto unica y exclusivamente del trabajo de los autores declarados anteriormente.
 */
include_once '../fragments/header.php';
include_once '../classes/OAuthUtils.php';

session_start ();
$_SESSION ['state'] = rand ();
$QUERY = OAuthUtils::$GOOGLE_OAUTH_URL . '?' . http_build_query ( array (
		'client_id' => OAuthUtils::$CLIENT_ID,
		'response_type' => OAuthUtils::$DEFAULT_RESPONSE_TYPE,
		'redirect_uri' => OAuthUtils::$REDIRECT_URI_CALLBACK,
		'scope' => OAuthUtils::$DEFAULT_SCOPE,
		'state' => $_SESSION ['state']
) );

?>

<body>
	<?php include_once '../fragments/site_title.php';?>

	<form name="user" method="post" action="session_init.php">

        <table class="login" border="1">
            <tr>
                <td width="50%">
                    <div align="center">Usuario</div>
                </td>
                <td>
                    <div align="center">
                        <input type="text" name="user" size="10"
                            required pattern="[A-Za-Z0-9]{5,8}">
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    <div align="center">Password</div>
                </td>
                <td>
                    <div align="center">
                        <input type="password" name="pswd" size="10"
                            required pattern="[A-Za-Z0-9]{5,8}">
                    </div>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <div align="center">
                        <input type="submit" name="login" value="Login">
                        <input type="button" name="register"
                            value="Registrarse"
                            onclick="location.href='register.php'"> <a
                            href="<?=$QUERY?>"><img height="20%"
                            alt="Log in With Google"
                            src="https://developers.google.com/accounts/images/sign-in-with-google.png"></a>
                    </div>
                </td>
            </tr>


        </table>

    </form>
<?php include_once '../fragments/footer.php';?>

</body>

</html>