<?php
/*
Asignatura: Gestion de la Informacion en la Web.
Practica:   4.
Grupo nº:   10.
Autores:
    Naji, Shahad.
    Pérez, Alexandra.
    Pax, Rafael.
    Vasquez, Oscar David.
El codigo a continuacion es fruto unica y exclusivamente del trabajo de los autores declarados anteriormente.
*/
	require_once 'sessions.php';
	$objses = new Sessions();
	$objses->init();
	$objses->destroy();
	header("Location: login.php")
?>