<?php 
/*
Asignatura: Gestion de la Informacion en la Web.
Practica:   4.
Grupo nº:   10.
Autores:
    Naji, Shahad.
    Pérez, Alexandra.
    Pax, Rafael.
    Vasquez, Oscar David.
El codigo a continuacion es fruto unica y exclusivamente del trabajo de los autores declarados anteriormente.
*/
include_once '../fragments/header.php';?>
<body>
	<?php include_once '../fragments/site_title.php';?>
	<?php

require_once '../users/sessions.php';

$objses = new Sessions();
$objses->init();

$user = isset($_SESSION['user']) ? $_SESSION['user'] : null ;

if($user != ''){

	$dni = $_SESSION['dni'];
	$nombre = $_SESSION['nombre'];
	$apellido1=$_SESSION['apellido1'];
	$apellido2=$_SESSION['apellido2'];
	$email=$_SESSION['email'];
	$pswd=$_SESSION['pswd'];
	$rol=$_SESSION['rol'];
?>
<form name="registro" method="post" action="valida.php?accion=modificar">  
<?php }
else{

	$dni = '';
	$nombre ='';
	$apellido1='';
	$apellido2='';
	$email='';
	$pswd='';
	$rol='';
?>
<form name="registro" method="post" action="valida.php?accion=registrar">
<?php } ?>

	
		

		<table class="login" border="1">
			<tr>
				<td width="50%">
					<div align="left">Nombre</div>
				</td>
				<td>
					<div align="left">


						<input type="text" name="nombre" value="<?=$nombre?>"size="50" required
						pattern="[A-Za-Z]" title="Introduzca nombre valido. Sin caracteres especiales">
					</div>
				</td>
			</tr>
			<tr>
				<td>
					<div align="left">Primer apellido</div>
				</td>
				<td>
					<div align="left">
						<input type="text" name="apellido1" value="<?=$apellido1?>" size="50" required
						pattern="[A-Za-Z]" title="Introduzca apellido valido. Sin caracteres especiales">
					</div>
				</td>
			</tr>
			<tr>
				<td>
					<div align="left">Segundo apellido</div>
				</td>
				<td>
					<div align="left">
						<input type="text" name="apellido2" value="<?=$apellido2?>"size="50" required
						pattern="[A-Za-Z]" title="Introduzca apellido valido. Sin caracteres especiales">
					</div>
				</td>
			</tr>
			<tr>
				<td>
					<div align="left">DNI</div>
				</td>
				<td>
					<div align="left">
						<input type="text" name="dni" size="50" value="<?=$dni?>" required placeholder=" 9 caracteres y letra en mayuscula"
						pattern="[A-Z0-9]{9}" title="Formato valido: 12345678A">
						
					</div>
				</td>
			</tr>
			<tr>
				<td>
					<div align="left">Email</div>
				</td>
				<td>
					<div align="left">
						
						<?php if($user == ''){?>
						<input type="email" name="email" value="<?=$email?>"size="50" required>
						<?php } else{?>
						<input type="email" name="email" value="<?=$email?>"size="50" readonly>
						<?php } ?>
					</div>
				</td>
			</tr>
			<tr>
				<td>
					<div align="left">Usuario</div>
				</td>
				<td>
					<div align="left">
						<input type="text" name="user" value="<?=$user?>"size="50" required
						pattern="[A-Za-Z0-9]{5,8}" title="Introduzca entre 5 y 8 caracteres. Solo letras o numeros">
					</div>
				</td>
			</tr>
			<tr>
				<td>
					<div align="left">Password</div>
				</td>
				<td>
					<div align="left">
						
						<?php if($user != ''){ ?>
							<input type="password" name="password" value=""
							size="50" required  placeholder ="Escriba nuevamente su password" 
							pattern="[A-Za-Z0-9]{5,8}" title="Puede ser diferente. Introduzca entre 5 y 8 caracteres. Solo letras y numeros">
						<?php } else{ ?>
						<input type="password" name="password" value=""
							size="50" required  placeholder ="Entre 5 y 8 caracteres" 
							pattern="[A-Za-Z0-9]{5,8}" title="Introduzca entre 5 y 8 caracteres. Solo letras y numeros">
						<?php } ?>
					</div>
				</td>
			</tr>
			<tr>
				<td>
					<div align="left">Rol</div>
				</td>
				<td>
					<div align="center">
						
						<?php if($user != ''){
							if($rol == 'Administrador'){ ?>
								<input type="radio" name="rol" value="Cliente"/> Cliente
								<input type="radio" name="rol" value="Administrador" value="Administrador" checked="checked"/> Administrador
						<?php } else{ ?>
							<input type="radio" name="rol" value="Cliente" checked="checked"/> Cliente
							<input type="radio" name="rol" value="Administrador" value="Administrador" /> Administrador
						<?php } 
						} else{ ?>
						<input type="radio" name="rol" value="Cliente" checked="checked"/> Cliente
						<input type="radio" name="rol" value="Administrador" value="Administrador"/> Administrador
						<?php } ?>
					</div>
				</td>
			</tr>	
			<tr>
				<td colspan="2">
					<div align="center">
						<?php if($user == ''){ ?>
						<input type="submit" value="Registrarse !">
						<?php } else { ?>
						<input type="submit" value="Modifica">
						<?php } ?>
						<input type="button" value="Volver" onclick="history.back(1)">
					</div>
				</td>
			</tr>

		</table>

	</form>
	<?php include_once '../fragments/footer.php';?>
</body>

</html>