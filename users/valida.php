<?php
/*
 * Asignatura: Gestion de la Informacion en la Web.
 * Practica: 4.
 * Grupo nº: 10.
 * Autores:
 * Naji, Shahad.
 * Pérez, Alexandra.
 * Pax, Rafael.
 * Vasquez, Oscar David.
 * El codigo a continuacion es fruto unica y exclusivamente del trabajo de los autores declarados anteriormente.
 */
include_once '../fragments/Constants.php';
include_once '../classes/LogInUtils.php';
include_once '../database/DBHelper.php';
include_once '../classes/Inputs.php';
// da igual
$accion = $_REQUEST ['accion'];
// nombre usuario-> filtro
$nombre = Inputs::checkUsuario ( $_POST ['nombre'] ) ? $_POST ['nombre'] : null;
// Un apellido va en el mismo saco que el usuario
$ape1 = Inputs::checkUsuario ( $_POST ['apellido1'] ) ? $_POST ['apellido1'] : null;
$ape2 = Inputs::checkUsuario ( $_POST ['apellido2'] ) ? $_POST ['apellido2'] : null;
$dni = Inputs::checkDNI ( $_POST ['dni'] ) ? $_POST ['dni'] : null;
$email = Inputs::checkEmail ( $_POST ['email'] ) ? $_POST ['email'] : null;
$user = Inputs::checkUsuario ( $_POST ['user'] ) ? $_POST ['user'] : null;
$pass = Inputs::sanitizeInput ( $_POST ['password'] ) ? $_POST ['password'] : null;
$rol = $_POST ['rol'] == 'Administrador' ? 'Administrador' : 'Cliente';

$data = [ 
		$accion,
		$nombre,
		$ape1,
		$ape2,
		$dni,
		$email,
		$user,
		$pass,
		$rol 
];
foreach ( $data as $key => $value ) {
	if ($value == null) {
		exit ( 'Valores no validos. Por favor, revisa las cosas.' );
	}
}

$objDb = new DBHelper ();

$emailExist = $objDb->existeEmail ( $email );

$usrExist = $objDb->existeUsuario ( $user );

if ($accion == "registrar") {
	if (! empty ( $emailExist )) {
		echo ' Atencion, ya existe un usuario con el correo: ' . $email;
	} else if (! empty ( $usrExist )) {
		echo 'Ya existe ese usuario en el sistema';
	} else {
		$info = [ 
				'dni' => $dni,
				'nombre' => $nombre,
				'apellido1' => $ape1,
				'apellido2' => $ape2,
				'email' => $email,
				'usuario' => $user,
				'password' => $pass,
				'rol' => $rol ,
				'googleAuth'=>false
		];
		$objDb->altaUsuario ( $info );
		
		header ( "Location: login.php" );
	}
} 

else {
	
	$info = [ 
			'nombre' => $nombre,
			'apellido1' => $ape1,
			'apellido2' => $ape2,
			'dni' => $dni,
			'usuario' => $user,
			'password' => $pass,
			'rol' => $rol ,
			'googleAuth'=>false
	];
	
	$objDb->modificaUsuario ( $info, $email );
	
	header ( "Location: logout.php" );
}

?>