<?php
/*
Asignatura: Gestion de la Informacion en la Web.
Practica:   4.
Grupo nº:   10.
Autores:
    Naji, Shahad.
    Pérez, Alexandra.
    Pax, Rafael.
    Vasquez, Oscar David.
El codigo a continuacion es fruto unica y exclusivamente del trabajo de los autores declarados anteriormente.
*/
class Sessions{
	
	public function __construct(){ }
	
	public function init(){
		@session_start();
		session_regenerate_id();
		// ini_set( 'session.use_only_cookies', TRUE );
		// ini_set( 'session.use_trans_sid', FALSE );
		// ini_set( 'session.cookie_lifetime', 1200 );
	}
	
	public function set($varname, $value){
		
		$_SESSION[$varname] = $value;
		
	}
	public function get($varname){
		return $_SESSION[$varname];
	}
	public function destroy(){
		
		session_unset();
		session_destroy();
		
	}
	
}

?>