<?php
/*
Asignatura: Gestion de la Informacion en la Web.
Practica:   4.
Grupo nº:   10.
Autores:
    Naji, Shahad.
    Pérez, Alexandra.
    Pax, Rafael.
    Vasquez, Oscar David.
El codigo a continuacion es fruto unica y exclusivamente del trabajo de los autores declarados anteriormente.
*/
include_once '../fragments/Constants.php';
class EntradaBD {
	private $Id;
	private $Id_teatro;
	private $nombre_teatro;
	private $nombre_obra;
	private $sesion;
	private $fila;
	private $asiento;
	private $dia;
	private $dni;
	
	public function EntradaBD ($Id,$Id_teatro,$nombre_teatro,$nombre_obra,$sesion,$fila,$asiento,$dia,$dni){
		$this->Id=$Id;
		$this->Id_teatro=$Id_teatro;
		$this->nombre_teatro=$nombre_teatro;
		$this->nombre_obra=$nombre_obra;
		$this->sesion=$sesion;
		$this->fila=$fila;
		$this->asiento=$asiento;
		$this->dia=$dia;
		$this->dni=$dni;
	}
	public function getId() {
		return $this->Id;
	}

	public function getId_teatro(){
		return $this->Id_teatro;
	}
	public function getNombre_teatro() {
		return $this->nombre_teatro;
	}
	public function getNombre_obra() {
		return $this->nombre_obra;
	}
	public function getSesion(){
		return $this->sesion;
	}
	public function getFila(){
		return $this->fila;
	}
	public function getAsiento(){
		return $this->asiento;
	}
	public function getDia(){
		return $this->dia;
	}
	public function getDni(){
		return $this->dni;
	}
	

	
}

?>