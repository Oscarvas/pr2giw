<?php
/*
 * Asignatura: Gestion de la Informacion en la Web.
 * Practica: 4.
 * Grupo nº: 10.
 * Autores:
 * Naji, Shahad.
 * Pérez, Alexandra.
 * Pax, Rafael.
 * Vasquez, Oscar David.
 * El codigo a continuacion es fruto unica y exclusivamente del trabajo de los autores declarados anteriormente.
 */
require_once '../fragments/Constants.php';
require_once '../database/DBHelper.php';
require_once 'EntradaBD.php';
require_once 'Inputs.php';
class LogInUtils {
	/**
	 * Comprueba si un usuario es valido
	 *
	 * @param unknown $userName        	
	 * @param unknown $password        	
	 * @return boolean
	 */
	
	// TODO porque **public** ?¿
	public static $objSe;
	public $result;
	public $rows;
	public function __construct() {
		$this->objSe = new Sessions ();
	}
	public function login_in() {
		if (Inputs::checkUsuario ( $_POST ["user"] ) && Inputs::sanitizeInput ( $_POST ["pswd"] )) {
			$this->rows = DBHelper::userExist ( $_POST ["user"], $_POST ["pswd"] );
		} else {
			header ( 'Location: ../users/login.php' );
			return;
		}
		if (! empty ( $this->rows )) {
			
			$this->objSe->init ();
			$this->objSe->set ( 'user', $this->rows ["usuario"] );
			$this->objSe->set ( 'nombre', $this->rows ["nombre"] );
			$this->objSe->set ( 'dni', $this->rows ["dni"] );
			$this->objSe->set ( 'apellido1', $this->rows ["apellido1"] );
			$this->objSe->set ( 'apellido2', $this->rows ["apellido2"] );
			$this->objSe->set ( 'email', $this->rows ["email"] );
			$this->objSe->set ( 'pswd', $this->rows ["password"] );
			$this->objSe->set ( 'rol', $this->rows ["rol"] );
			header ( 'Location: ../index.php' );
		} else {
			header ( 'Location: ../users/login.php' );
		}
	}
	public static function isValidUser($session) {
		$valid = true; // esto tiene pinta de estar haciendo algo mal
		if (isset ( $session [VALID_FLAG] ) && $session [VALID_FLAG] == 'true') {
			$valid = true;
		}
		return $valid;
	}
	public static function misEntradas() {
		$email = $_SESSION ['email'];
		$array_entradas = [ ];
		$result = DBHelper::entradas ( $email );
		
		while ( $result->hasNext () ) {
			$obj = $result->getNext ();
			$infoTeatro = DBHelper::nombreObraTeatro ( $obj ['Id_teatro'] );
			$info = $infoTeatro->getNext ();
			
			array_push ( $array_entradas, new EntradaBD ( $obj ['_id'], $obj ['Id_teatro'], $info ['nombre_teatro'], $info ['nombre_obra'], $obj ['sesion'], $obj ['fila'], $obj ['asiento'], $obj ['dia'], $_SESSION ['email'] ) );
		}
		
		unset ( $obj );
		return $array_entradas;
	}
	public static function heValorado($Id_teatro) {
		$dni = $_SESSION ['dni'];
		$cnt = DBHelper::numValoradas ( $Id_teatro, $dni );
		
		return $cnt;
	}
}

?>