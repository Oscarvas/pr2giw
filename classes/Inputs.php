<?php
/*
 * Asignatura: Gestion de la Informacion en la Web.
 * Practica: 4.
 * Grupo nº: 10.
 * Autores:
 * Naji, Shahad.
 * Pérez, Alexandra.
 * Pax, Rafael.
 * Vasquez, Oscar David.
 * El codigo a continuacion es fruto unica y exclusivamente del trabajo de los autores declarados anteriormente.
 */
class Inputs {
	public static function sanitizeInput($input) {
		// strip_tags() quitar etiqutas html y php
		// htmlspecialchars convierte caracteres especiales en entidades HTML
		// trim elimina los espacios en blanco iniciales y finales deñ string
		$result = htmlspecialchars ( trim ( strip_tags ( $input ) ) );
		return $result;
	}
	
	// eliminar caracteres raros
	public static function sanitizeChar($input) {
		$textoLimpio = preg_replace ( '([^A-Za-z0-9])', ' ', $input );
		return $textoLimpio;
	}
	public static function sanitizeDigitos($input) {
		$textoLimpio = preg_replace ( '/\D/', '', $input );
		return $textoLimpio;
	}
	
	// Comprobar nombre, apellido1 y apellido2
	public static function checkUsuario($input) {
		return (ereg_replace ( '[^ A-Za-z0-9_-ñÑ]', '', $input ));
	}
	
	// Comprobar documento de identificacion
	public static function checkDNI($input) {
		return preg_match("/(([X-Z]{1})([-]?)(\d{7})([-]?)([A-Z]{1}))|((\d{8})([-]?)([A-Z]{1}))/",
		 $input, $output_array)>0;
	}
	// Comprobar email
	public static function checkEmail($email) {
		if ((filter_var ( $email, FILTER_VALIDATE_EMAIL )) && (strlen ( $email ) < 100)) {
			return true;
		} else
			return false;
	}
	
	// Comprobar fecha
	public static function checkFecha($input) {
		$df = DateTime::createFromFormat ( 'Y-m-d', $input );
		return $df && $df->format ( 'Y-m-d' ) == $input && new DateTime("now")<=$df;
	}
	// Comprobacion del campo puntuacion en realidad no hace falta porque la comprobacion la realiza el propio input
	public static function checkPuntuacion($input) {
		$options = array (
				'options' => array (
						'min_range' => 1,
						'max_range' => 5 
				) 
		);
		if (! filter_var ( $input, FILTER_VALIDATE_INT, $options )) {
			return false;
		}
		return true;
	}
	
	// comprobar la longitud de comentario y de descripción
	public static function checkTextLen($input) {
		if (strlen ( $input ) > 200) {
			
			return false;
		} elseif (empty ( $input )) {
			
			return false;
		}
		return true;
	}
	
	// Comprobar la long de nfilas y nAsientos
	public static function checkDigLen($input) {
		if (strlen ( $input ) > 2) {
			return false;
		} else {
			return true;
		}
	}
	
	// Comprobar la longitud: nombre teatro y nombre obra
	public static function checkTeaLen($input) {
		if (strlen ( $input ) > 100) {
			
			return false;
		}
		return true;
	}
	
	// Comprobar los campos de sesión
	public static function checkHora($input) {
		if (strtotime ( $input )) {
			return true;
		}
		return false;
	}
}

?>