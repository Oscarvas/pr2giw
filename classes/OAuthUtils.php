<?php
class OAuthUtils {
	public static $GOOGLE_OAUTH_URL = 'https://accounts.google.com/o/oauth2/auth';
	public static $CLIENT_ID = '767084801348-2i2katho5j8a9s1vos0g9mrshm78cn95.apps.googleusercontent.com';
	public static $REDIRECT_URI_CALLBACK = 'http://localhost/src/oauth/oauthcallback.php';
	public static $REDIRECT_URI_TOKEN_HANDLER = 'http://localhost/src/oauth/oauthTokenHandler.php';
	public static $DEFAULT_SCOPE = 'openid email profile';
	public static $DEFAULT_RESPONSE_TYPE = 'code';
	public static $GOOGLEAPIS_OAUTH2_V3_TOKEN_URL = 'https://www.googleapis.com/oauth2/v3/token';
	public static $CLIENT_SECRET = 'BJBvhFm8cV6H6BrSzMbvVbx8';
	public static $GOOGLE_CERT_URL = 'https://www.googleapis.com/oauth2/v1/certs';
	private static $GOOGLE_OPENID_CONFIGURATION_URL = 'https://accounts.google.com/.well-known/openid-configuration';
	public static $GOOGLE_PLUS_ME_URL = 'https://www.googleapis.com/plus/v1/people/me';
	private static $GOOGLE_OPENID_CONFIGURATION_PARAMS = null;
	public static $API_KEY = "AIzaSyDLD3abmQwZYcFcL2fFrBtqfmB7KNYdD6Q";
	public static function getGoogleOpenIdConfig() {
		if (self::$GOOGLE_OPENID_CONFIGURATION_PARAMS == null) {
			$GOOGLE_OPENID_CONFIGURATION_PARAMS = json_decode ( file_get_contents ( $GOOGLE_OPENID_CONFIGURATION_URL, false ) );
		}
		return $GOOGLE_OPENID_CONFIGURATION_PARAMS;
	}
}

?>