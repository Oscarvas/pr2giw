<?php
/*
Asignatura: Gestion de la Informacion en la Web.
Practica:   4.
Grupo nº:   10.
Autores:
    Naji, Shahad.
    Pérez, Alexandra.
    Pax, Rafael.
    Vasquez, Oscar David.
El codigo a continuacion es fruto unica y exclusivamente del trabajo de los autores declarados anteriormente.
*/
include_once '../fragments/Constants.php';
include_once 'ValoracionBD.php';
class TeatroBD {
	private $Id;
	private $nombre_teatro;
	private $nombre_obra;
	private $descripcion;
	private $sesion1;
	private $sesion2;
	private $sesion3;
	private $nume_filas;
	private $nume_asientos;
	private $array_valoracion;
	
	
	/**
	 *
	 * @param number $id
	 *        	el identificador de un teatro
	 * @return TeatroBD un TeatroBD con los atributos rellenados
	 */

	public function TeatroBD($Id, $nombre_teatro, $nombre_obra, $descripcion, $sesion1, $sesion2, $sesion3, $nume_filas, $nume_asientos) {
		$this->Id = $Id;
		$this->nombre_teatro = $nombre_teatro;
		$this->nombre_obra = $nombre_obra;
		$this->descripcion = $descripcion;
		$this->sesion1 = $sesion1;
		$this->sesion2 = $sesion2;
		$this->sesion3 = $sesion3;
		$this->nume_filas = $nume_filas;
		$this->nume_asientos = $nume_asientos;
		$this->array_valoracion = [];
		
	}

	public static function getTeatro($idTeatro) {
		
		$teatro = DBHelper::readTeatro($idTeatro);

		if (empty ($teatro))
			$t = NULL;
		else
			$t = new TeatroBD ( $teatro['_id'],
								$teatro['nombre_teatro'],
								$teatro['nombre_obra'],
								$teatro['descripcion'],
								$teatro['sesion1'],
								$teatro['sesion2'],
								$teatro['sesion3'],
								$teatro['nume_filas'],
								$teatro['nume_asientos'] );
		return $t;
	}
	public static function search($str, $type) {
		
		if ($type == SEARCH_TERM_OBRA) {
			$teatros = DBHelper::buscaObra($str);
		} else if ($type == SEARCH_TERM_TEATRO) {
			$teatros = DBHelper::buscaTeatro($str);
		} else {
			return [];
		}
		
		$results = [ ];
		while ( $teatros->hasNext() ) {
			$t = $teatros->getNext();
			array_push ( $results, new TeatroBD(
									$t['_id'],
									$t['nombre_teatro'],
									$t['nombre_obra'],
									$t['descripcion'],
									$t['sesion1'],
									$t['sesion2'],
									$t['sesion3'],
									$t['nume_filas'],
									$t['nume_asientos'] ) );
		}
		return $results;
	}
	public static function getAllTeatros() {
		
		$cursor = DBHelper::allTeatros();
		$results = [];
		while ( $cursor->hasNext() ) {
			$t = $cursor->getNext();
			array_push ( $results, new TeatroBD(
									$t['_id'],
									$t['nombre_teatro'],
									$t['nombre_obra'],
									$t['descripcion'],
									$t['sesion1'],
									$t['sesion2'],
									$t['sesion3'],
									$t['nume_filas'],
									$t['nume_asientos'] ) );
		}
		return $results;
	}
	public function getArray_valoracion(){
		
		$result = DBHelper::getValoraciones($this->Id);
		while ($result->hasNext() ) {
			$val = $result->getNext();
			array_push ( $this->array_valoracion, 
					new ValoracionBD ( 
										$val['_id'],
										$this->Id,
										$val['dni'],
										$val['dia'],
										$val['puntuacion'],
										$val['comentario'] ) );
		}
		return $this->array_valoracion;



	}

	/**
	 * Devuelve el numero de obras existentes en la BD
	 *
	 * @return number el num de obras
	 */
	public static function numeroDeObras() {
		
		return DBHelper::numObras();
	}
	public function asiento($fila,$asiento,$sesion,$dia) {
		$numero='no';

		$result =DBHelper::getAsiento($this->Id,$fila,$asiento,$sesion,$dia);
		 if(!empty($result)){
			$numero=$result['email'];
		}
		
		return $numero;
	}

	/**
	 * Borra un teatro dado su identificador
	 *
	 * @param unknown $identificador        	
	 */
	public static function deleteTeatro($identificador) {
		DBHelper::borrarTeatro($identificador);
	}
	/**
	 *
	 * @param TeatroBD $teatro        	
	 */
	public static function addOrModifyTeatro($t) {		
		
		if ($t->Id >= 0) {
			$update = [ 
			'_id'=> new MongoId($t->Id), 
			'nombre_teatro'=> (string) $t->nombre_teatro, 
			'nombre_obra'=> (string) $t->nombre_obra, 
			'descripcion'=> (string) $t->descripcion, 
			'sesion1'=> (string) $t->sesion1, 
			'sesion2'=> (string) $t->sesion2, 
			'sesion3' => (string) $t->sesion3, 
			'nume_filas'=> (string) $t->nume_filas, 
			'nume_asientos'=> (string) $t->nume_asientos ];
			DBHelper::modifyTeatro($update);

		} else {
			$insert = [ 
			'nombre_teatro'=> (string) $t->nombre_teatro, 
			'nombre_obra'=> (string) $t->nombre_obra , 
			'descripcion'=> (string) $t->descripcion, 
			'sesion1'=> (string) $t->sesion1, 
			'sesion2'=> (string) $t->sesion2, 
			'sesion3' => (string) $t->sesion3, 
			'nume_filas'=> (string) $t->nume_filas , 
			'nume_asientos'=> (string) $t->nume_asientos ];

			DBHelper::createTeatro($insert);

		}		
	}

	public function getId() {
		return $this->Id;
	}
	public function getNombre_teatro() {
		return $this->nombre_teatro;
	}
	public function getNombre_obra() {
		return $this->nombre_obra;
	}
	public function getDescripcion() {
		return $this->descripcion;
	}
	public function getSesion1() {
		return $this->sesion1;
	}
	public function getSesion2() {
		return $this->sesion2;
	}
	public function getSesion3() {
		return $this->sesion3;
	}
	public function getNume_fila() {
		return $this->nume_filas;
	}
	public function getNume_asientos() {
		return $this->nume_asientos;
	}
}
?>