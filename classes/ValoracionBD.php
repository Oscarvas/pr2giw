<?php 
/*
Asignatura: Gestion de la Informacion en la Web.
Practica:   4.
Grupo nº:   10.
Autores:
    Naji, Shahad.
    Pérez, Alexandra.
    Pax, Rafael.
    Vasquez, Oscar David.
El codigo a continuacion es fruto unica y exclusivamente del trabajo de los autores declarados anteriormente.
*/
include_once '../fragments/Constants.php';
include_once '../database/DBHelper.php';

Class ValoracionBD{

	private $Id;
	private $Id_teatro;
	private $dni;
	private $dia;
	private $puntuacion;
	private $comentario;
	
	
	public function ValoracionBD($Id,$Id_teatro,$dni,$dia,$puntuacion,$comentario){
		$this->Id=$Id;
		$this->Id_teatro=$Id_teatro;
		$this->dni=$dni;
		$this->dia=$dia;
		$this->puntuacion=$puntuacion;
		$this->comentario=$comentario;
		

	}

	public function getId() {
		return $this->Id;
	}
	public function getId_teatro(){
		return $this->Id_teatro;
	}
	public function getDni(){
		return $this->dni;
	}
	public function getDia(){
		return $this->dia;
	}
	public function getPuntuacion() {
		return $this->puntuacion;
	}
	public function getComentario() {
		return $this->comentario;
	}


	public function addValoracion(){
		$datos = [
			'Id_teatro' => new MongoId ($this->Id_teatro), 
			'dni' => $this->dni , 
			'dia' => $this->dia , 
			'puntuacion' => $this->puntuacion , 
			'comentario' => $this->comentario ];

		DBHelper::newValoracion($datos);
		
	}
		
}

?>