<?php
/*
Asignatura: Gestion de la Informacion en la Web.
Practica:   4.
Grupo nº:   10.
Autores:
    Naji, Shahad.
    Pérez, Alexandra.
    Pax, Rafael.
    Vasquez, Oscar David.
El codigo a continuacion es fruto unica y exclusivamente del trabajo de los autores declarados anteriormente.
*/

/**
* 
*/
class Crypto 
{
	
	function __construct(){}

	public static function salGenerator(){
		$long = rand(7,14);
		$bin = openssl_random_pseudo_bytes($long,$cstrong);
		$sal = bin2hex($bin);

		return $sal;
	}

	public static function encrypt($value){
		$crypt = hash('ripemd256', $value, false);
		return $crypt;
	}
}

?>