<?php
/*
Asignatura: Gestion de la Informacion en la Web.
Practica:   4.
Grupo nº:   10.
Autores:
    Naji, Shahad.
    Pérez, Alexandra.
    Pax, Rafael.
    Vasquez, Oscar David.
El codigo a continuacion es fruto unica y exclusivamente del trabajo de los autores declarados anteriormente.
*/

require_once 'users/sessions.php';
$objses = new Sessions();
$objses->init();

$user = isset($_SESSION['user']) ? $_SESSION['user'] : null ;

if($user == ''){
    header('Location: users/login.php');
}

?>


<?php include_once 'fragments/header.php';?>

<body>

    <?php include_once'fragments/site_title.php';?>
    <table class="main">
        <tr>
            <?php include_once'fragments/menu_left.php';?>
            <td class="right">
                <h1><?php echo "Bienvenido, " . html_entity_decode(htmlentities($_SESSION['nombre']));?></h1>

                <table class="content">Usa el men&uacute; de
                    navegaci&oacute;n de la izquierda para realizar las
                    operaciones.
                </table>
            </td>
            <td width="21%" valign="top"></td>
        </tr>
    </table>
    <?php include_once'fragments/footer.php';?>


</body>

</html>