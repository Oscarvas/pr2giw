<?php
include_once '../classes/OAuthUtils.php';
include_once '../database/DBHelper.php';
session_start ();

if (! isset ( $_SESSION ['state'] ) || $_SESSION ['state'] != $_GET ['state'])
	header ( 'Location: ../users/login.php' );

$encoded_token = file_get_contents ( OAuthUtils::$GOOGLEAPIS_OAUTH2_V3_TOKEN_URL, false, stream_context_create ( array (
		'http' => array (
				'header' => "Content-type: application/x-www-form-urlencoded\r\n",
				'method' => 'POST',
				'content' => http_build_query ( array (
						'code' => $_GET ['code'],
						'client_id' => OAuthUtils::$CLIENT_ID,
						'client_secret' => OAuthUtils::$CLIENT_SECRET,
						'redirect_uri' => OAuthUtils::$REDIRECT_URI_CALLBACK,
						'grant_type' => 'authorization_code' 
				) ) 
		) 
) ) );

$decoded_token = json_decode ( $encoded_token, true );
$payload = json_decode ( base64_decode ( explode ( '.', $decoded_token ['id_token'] )[1] ), true );

if (! isset ( $payload ['email_verified'] ))
	header ( 'Location: ../users/login.php' );

$googlePlusData = json_decode ( file_get_contents ( OAuthUtils::$GOOGLE_PLUS_ME_URL . '?' . http_build_query ( array (
		'access_token' => $decoded_token ['access_token'],
		'key' => OAuthUtils::$API_KEY 
) ), false, stream_context_create ( array (
		'http' => array (
				'header' => "Content-type: application/x-www-form-urlencoded\r\n" . "Authorization: Bearer " . $decoded_token ['access_token'] . "\r\n" 
		) 
) ) ) );
// ahora tenemos el mail verificado. Comprobamos que el usuario existe. Si no, lo enchufamos
if (! DBHelper::existeEmail ( $payload ['email'] )) {
	// Insertar en la BD los datos de...Google+
	DBHelper::altaUsuario ( [ 
			'dni' => $payload ['email'],
			'nombre' => $googlePlusData->displayName,
			'apellido1' => $googlePlusData->familyName,
			'apellido2' => "",
			'email' => $payload ['email'],
			'usuario' => $payload ['email'],
			'password' => rand (),
			'rol' => 'Cliente',
			'googleAuth' => true 
	] );
}
$_SESSION ['user'] = $payload ['email'];
$_SESSION ['nombre'] = $googlePlusData->displayName;
$_SESSION ['dni'] = $googlePlusData->id;
$_SESSION ['apellido1'] = $googlePlusData->familyName;
$_SESSION ['apellido2'] = "";
$_SESSION ['email'] = $payload ['email'];
$_SESSION ['pswd'] = rand ();
$_SESSION ['rol'] = 'Cliente';
$_SESSION ['googleAuth'] = true;
header ( 'Location: ../index.php' );
?>