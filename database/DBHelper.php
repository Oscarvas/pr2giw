<?php
/*
Asignatura: Gestion de la Informacion en la Web.
Practica:   4.
Grupo nº:   10.
Autores:
    Naji, Shahad.
    Pérez, Alexandra.
    Pax, Rafael.
    Vasquez, Oscar David.
El codigo a continuacion es fruto unica y exclusivamente del trabajo de los autores declarados anteriormente.
*/
include_once '../classes/Crypto.php';

Class DBHelper {
	public function __construct(){ }

	public static function conectaMongo($coleccion){
		$conexion = new MongoClient();
		$conect = $conexion->GIW_grupo10->$coleccion; //esperaba poder poner datos del Constants
		return $conect; //devuelve la conexion a la coleccion que queramos definida en $coleccion
	}

	//////////////////////////// Metodos valoracion ////////////////////////////

	public static function newValoracion($data){

		$conexion = self::conectaMongo('valoracion');
		$conexion->insert($data);
	}

	public static function getValoraciones($id){
		$conexion = self::conectaMongo('valoracion');
		$result = $conexion->find(array('Id_teatro' => new MongoId($id)));
		return $result;
	}

	public static function numValoradas( $id, $dni){

		$consulta = self::conectaMongo('valoracion');
		$result = $consulta->find(array ('Id_teatro' => new MongoId($id) , 'dni'=>$dni))->count();
		return $result;
	}

	//////////////////////////// Metodos seguridad ////////////////////////////
	// en este agregado el campo usuario contendra el correo en lugar del usuario de acceso

	private static function addSecurity($usr){

		$consulta =self::conectaMongo('seguridad');
		$codif = array('usuario' => $usr,'sal' => Crypto::salGenerator());
		$consulta->insert($codif);

	}

	private static function seekSal($usr){
		$consulta =self::conectaMongo('seguridad');
		$result = $consulta->findOne(array('usuario' => (string) "$usr"));
		return $result['sal'];
	}

	private static function securePass($psw, $sal){
		$psw .= $sal;
		return Crypto::encrypt($psw);
	}

	private static function updateSal($email, $psw){ //voy a recibir la pass sin codificar

		$sal = self::seekSal($email);
		$coded = self::securePass($psw , $sal);

		return $coded;

	}


	//////////////////////////// Metodos usuario ////////////////////////////
	private static function getEmail($usr){
		$consulta = self::conectaMongo('usuario');
		$result = $consulta->findOne(array('usuario' => "$usr"), array('email' => 1, '_id' => 0));
		return $result;
	}

	public static function userExist($usr, $psw){
		$consulta = self::conectaMongo('usuario');
		$email = self::getEmail($usr);
		$psw = self::securePass($psw, self::seekSal($email['email']));
		$result = $consulta->findOne(array ('usuario' => "$usr" , 'password'=>"$psw",'googleAuth'=>false));

		return $result;
	}

	public static function existeEmail($email){
		$consulta = self::conectaMongo('usuario');
		$result = $consulta->findOne(array('email'=>$email));
		return $result;
	}
	public static function isGoogleAuth($email){
		$consulta = self::conectaMongo('usuario');
		$result = $consulta->findOne(array('email'=>$email,'googleAuth'=>true));
		return $result;
	}
	public static function existeUsuario($usr){
		$consulta = self::conectaMongo('usuario');
		$result = $consulta->findOne(array('usuario' => "$usr" ));
		return $result;
	}

	public static function altaUsuario($data){
		$consulta = self::conectaMongo('usuario');
		self::addSecurity($data['email']);
		$data['password'] = self::updateSal($data['email'], $data['password']);;
		$consulta->insert($data);
	}

	public static function modificaUsuario($data,$email){
		$consulta = self::conectaMongo('usuario');
		//buscar si ha cambiado la pass
		if (self::passChanged($email,$data['password'])) {
			$data['password'] = self::updateSal($email , $data['password']);
		}
		
		$consulta->update( array("email" =>"$email"), array( '$set' => $data));
	}

	private static function passChanged($email, $psw){
		$consulta = self::conectaMongo('usuario');
		$result = $consulta->findOne(array('email' => $email ));
		$bool = true;

		if ($result['password'] == $psw ){
			$bool = false;
		}

		return $bool;

	}

	//////////////////////////// Metodos entradas ////////////////////////////

	public static function entradas($email){
		$consulta = self::conectaMongo('entradas');
		$result = $consulta->find(array ('email' => $email ));

		return $result;
	}

	public static function getAsiento($id,$fila,$asiento,$sesion,$dia){		
		$consulta = self::conectaMongo('entradas');
		$result = $consulta->findOne( array('Id_teatro' => new MongoId($id),
										 'sesion' =>  (string)  "$sesion",
										 'fila' =>  (string)  "$fila",
										 'asiento' =>  (string) "$asiento",
										 'dia'=> (string) "$dia" ));
		return $result;
	}

	public static function compra($info){
		$consulta = self::conectaMongo('entradas');
		$consulta->insert($info);
	}

	public static function venta($info){
		$consulta = self::conectaMongo('entradas');
		$consulta->remove($info);
	}

	//////////////////////////// Metodos Teatro ////////////////////////////

	public static function allTeatros(){
		$consulta =self::conectaMongo('teatro');
		$result = $consulta->find();
		return $result;
	}

	public static function buscaObra($busqueda){
		$consulta =self::conectaMongo('teatro');
		$result = $consulta->find(array('nombre_obra'=>  (string) $busqueda));
		return $result;
	}

	public static function buscaTeatro($busqueda){
		$consulta =self::conectaMongo('teatro');
		$result = $consulta->find(array('nombre_teatro'=> (string) $busqueda));
		return $result;
	}

	public static function nombreObraTeatro($id){
		$consulta = self::conectaMongo('teatro');
		$result = $consulta->find(array('_id' => new MongoId($id) ), array('nombre_teatro'=>1, 'nombre_obra'=>1, '_id'=>0));
		return $result;
	}
	
	public static function numObras(){
		$consulta = self::conectaMongo('teatro');
		return $consulta->find()->count();
	}

	public static function borrarTeatro($id){
		$consulta = self::conectaMongo('teatro');
		$consulta->remove(array('_id' => new MongoId($id)));
	}

	public static function createTeatro($data){
		$consulta = self::conectaMongo('teatro');
		$consulta->insert($data);
	}

	public static function modifyTeatro($data){
		$consulta = self::conectaMongo('teatro');
		$consulta->update(array ('_id'=>$data['_id']),$data);
	}

	public static function readTeatro($id){
		$consulta = self::conectaMongo('teatro');
		$result = $consulta->findOne(array('_id' => new MongoId($id) ));
		return $result;
	}
}

?>